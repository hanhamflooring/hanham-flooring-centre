Founded in 2006, we provide a vast array of flooring to customers in the Bristol area. From Carpets to Karndean, we have you covered. Check out the website and call us today to find out our latest products and offers.

Address: 46A High Street, Hanham, Bristol BS15 3DR, UK

Phone: +44 117 961 1122
